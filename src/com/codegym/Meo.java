package com.codegym;

import java.util.Comparator;

public class Meo implements Comparable<Meo> {

    private int id;
    private int age;

    public Meo() {
    }

    public Meo(int id, int age) {
        this.id = id;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Meo{" +
                "id=" + id +
                ", age=" + age +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    //    @Override
//    public int compare(Meo o1, Meo o2) {
//        return 0;
//    }
//    Override
//    public int compare(Meo o1, Meo o2) {
//        return 0;
//    }
//
    @Override
    public int compareTo(Meo o) {
        return 0;
    }

}
