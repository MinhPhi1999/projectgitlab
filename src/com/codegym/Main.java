package com.codegym;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Main {
    public static void main(String[] args) {
//
//                Synchronized.Table obj = new Synchronized.Table();// tao mot object
//                Synchronized.MyThread1 t1 = new Synchronized.MyThread1(obj);
//                Synchronized.MyThread2 t2 = new Synchronized.MyThread2(obj);
//                Synchronized.MyThread1 t3 = new Synchronized.MyThread1(obj);
//
//
//
//        t1.start();
//        try {
//            t1.join();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        t2.start();
//        try {
//            t2.join();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        t3.start();

        StringBuilder sb = new StringBuilder();
        sb.append("a");
        sb.append("a");
        sb.append("a");
        sb.append("a");
        System.out.println(sb);
        Meo meo = new Meo(1,20);

        System.out.println(meo);

        double num = 1.234543278d;
        String value = String.format("day la so %1.4f",num);
        System.out.println(value);
//        Animal animal = new Animal() {
//            @Override
//            public void keu() {
//                super.keu();
//            }
//
//            @Override
//            void run() {
//
//            }
//        };
//        animal.keu();
//        animal.name = "orgy";
//        System.out.println(animal.name);
//        Cat cat = new Cat() {
//            //override lại pthuc cũng đc nma bắt buộc phải có {}
////    @Override
////    public void keu() {
////        super.keu();
////    }
////
////    @Override
////    void run() {
////        super.run();
////    }
//        };
//        cat.keu();
//        try {
//            chia2so(2, 0);
//        } catch (MyException e) {
//            System.out.println(e.getError());
//        } finally {
//            System.out.println("helooo");
//        }
//
//        List<Integer> list = new ArrayList<>();
//        list.add(1);
//        list.add(2);
//        list.add(3);
//        list.add(4);
//        list.add(5);
//
////        for (int i = 0; i < list.size(); i++) {
////            if (list.get(i).equals(4)){
////                list.remove(list.get(i));
////            }
////            System.out.println(list.get(i));
////        }
//
//        Iterator<Integer> itr = list.iterator();
//        while (itr.hasNext()) {
//            Integer s = itr.next();
//            System.out.println(s);
//            if (s.equals(3)) {
//                itr.remove();
//            }
//        }
//        System.out.println(list);
//        System.out.println();
        Map<Integer, String> stdMap = new HashMap<>();
        stdMap.put(2,"b");
        stdMap.put(4,"h");
        stdMap.put(3,"s");
        stdMap.put(1,"q");
        stdMap.put(5,"a");
        System.out.println(stdMap);
//        Set<Integer> set = stdMap.keySet();
//        for (Integer key : set) {
//            System.out.println(key + " " + stdMap.get(key));
//        }
        for (Map.Entry<Integer, String> entry: stdMap.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
////        Iterator<Integer> iMap = stdMap.keySet().iterator();
////        while (iMap.hasNext()){
////            System.out.println(iMap.next());
////        }
////        HashSet<Integer> set1 = new HashSet<>();
////        set1.add(200);
////        set1.add(22);
////        set1.add(33);
////        set1.add(21);
////        for (Integer element: set1) {
////            System.out.println(element);
////        }
////        System.out.println(set1);
//        try {
//            validate(13);
//        } catch (ArithmeticException e) {
//            System.out.println(e);
//        }
//
//        System.out.println("abc");
//
//        Car car1 = new Car();
//        Car car2 = new Car();
//        car1.start();
//        car2.start();

        // string to date
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String str = "07-09-2022";
        try {
            Date date = sdf.parse(str);
            System.out.println(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Date date2 = new Date();
        String dts = sdf.format(date2);
        System.out.println(dts);
    }

    // hàm
    public static void chia2so(int a, int b) throws MyException {
        try {
            int kq = a / b;
            System.out.println(kq);
        } catch (Exception e) {
            throw new MyException("lỗi chia cho 0");
        }
    }

    static void validate(int age) {
        if (age < 18)
            throw new ArithmeticException("K đủ tuổi");
        else
            System.out.println("welcome to vote");
    }
}
